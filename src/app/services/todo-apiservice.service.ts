import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {Todo} from "../model/todo";

@Injectable({
  providedIn: 'root'
})
export class TodoAPIServiceService {

  private apiUrl = 'http://localhost:8080';

  constructor(private http: HttpClient) { }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
    })
  };

  getTodos() {
    return this.http.get(this.apiUrl);
  }

  submitTodo(todo: Todo): Observable<Todo>  {
    return this.http.post('http://localhost:8080/add', {"content":"Test2","status":false}, this.httpOptions);
  }


  toggleTodo(id: string) {
    const url = `${this.apiUrl}/toggle/${id}`;
    return this.http.put(url,null);
  }

  deleteList() {
    const url = `${this.apiUrl}/delete`;
    return this.http.delete(url);
  }
}

