import { TestBed } from '@angular/core/testing';

import { TodoAPIServiceService } from './todo-apiservice.service';
import {HttpClientModule} from "@angular/common/http";

describe('TodoAPIServiceService', () => {
  let service: TodoAPIServiceService;
  beforeEach(() => {
    TestBed.configureTestingModule({imports: [HttpClientModule], providers: [TodoAPIServiceService]});
    service = TestBed.inject(TodoAPIServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
