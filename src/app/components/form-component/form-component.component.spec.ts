import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormComponentComponent } from './form-component.component';
import {By} from "@angular/platform-browser";
import {HttpClientModule} from "@angular/common/http";
import {TodoAPIServiceService} from "../../services/todo-apiservice.service";

describe('FormComponentComponent', () => {
  let component: FormComponentComponent;
  let fixture: ComponentFixture<FormComponentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormComponentComponent ],
      imports: [HttpClientModule],
      providers: [TodoAPIServiceService]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should show submit button', () => {
    expect(fixture.nativeElement.querySelector('[data-test="submitButton"]').innerText).toBe('Submit');
  });

  it('should show lable for namefield', () => {
    expect(fixture.nativeElement.querySelector('[data-test="lableNamefield"]').innerText).toBe('Name: ');
  });

  it('should show name field', () => {
    expect(fixture.nativeElement.querySelector('[data-test="namefield"]')).toBeTruthy();
  });

  it('should run submit method on click', async () => {
    spyOn(component, 'submit');

    let button = fixture.debugElement.nativeElement.querySelector('[data-test="submitButton"]');
    button.click();

    fixture.whenStable().then(() => {
      expect(component.submit).toHaveBeenCalled();
    })
  });

  /*it('should bind input on name property', async () => {
    let input = fixture.debugElement.query(By.css('input'));
    let namefield = input.nativeElement
    fixture.whenStable().then(() => {
      namefield.value = 'Test'
      namefield.dispatchEvent(new Event('input'))
      expect(component.name).toEqual('Test');
    })
  });*/

});
