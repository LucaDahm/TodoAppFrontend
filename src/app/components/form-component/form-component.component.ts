import { Component, OnInit } from '@angular/core';
import {TodoAPIServiceService} from "../../services/todo-apiservice.service";
import {Todo} from "../../model/todo";

@Component({
  selector: 'app-form-component',
  templateUrl: './form-component.component.html',
  styleUrls: ['./form-component.component.css']
})
export class FormComponentComponent implements OnInit {
  name: string = '';

  constructor(private todoApiService: TodoAPIServiceService) {
  }



  ngOnInit(): void {
  }

  submit () {
    let todo = new Todo();
    this.todoApiService.submitTodo(todo);
    this.name = '';
  }

  clear() {
    this.todoApiService.deleteList();
  }
}
