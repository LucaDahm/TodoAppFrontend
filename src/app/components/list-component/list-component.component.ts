import { Component, OnInit } from '@angular/core';
import {Todo} from "../../model/todo";
import {TodoAPIServiceService} from "../../services/todo-apiservice.service";
import {Observable} from "rxjs";

@Component({
  selector: 'app-list-component',
  templateUrl: './list-component.component.html',
  styleUrls: ['./list-component.component.css']
})
export class ListComponentComponent implements OnInit {

  todos$: Observable<any>;

  constructor(public todoApiService: TodoAPIServiceService) {
    this.todos$ = this.todoApiService.getTodos();
  }

  toggle (id: string) {
    this.todoApiService.toggleTodo(id);
  }

  ngOnInit(): void {
  }

}
