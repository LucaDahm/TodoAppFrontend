import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { ListComponentComponent } from './components/list-component/list-component.component';
import { FormComponentComponent } from './components/form-component/form-component.component';
import {FormsModule} from "@angular/forms";
import {HttpClient, HttpClientModule} from "@angular/common/http";
import {TodoAPIServiceService} from "./services/todo-apiservice.service";
import {HttpClientTestingModule} from "@angular/common/http/testing";

@NgModule({
  declarations: [
    AppComponent,
    ListComponentComponent,
    FormComponentComponent
  ],
    imports: [
        BrowserModule,
        FormsModule,
        HttpClientModule,
    ],
  providers: [TodoAPIServiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
